package br.mvpcortes.exampleDATACMNS1343;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleDatacmns1343Application {

	public static void main(String[] args) {
		SpringApplication.run(ExampleDatacmns1343Application.class, args);
	}
}
