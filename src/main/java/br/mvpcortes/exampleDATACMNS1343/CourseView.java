package br.mvpcortes.exampleDATACMNS1343;

import org.springframework.beans.factory.annotation.Value;

public interface CourseView {

    @Value("${target.idStudent}")
    Integer getIdStudent();

    @Value("${target.nameStudent}")
    String getNameStudent();

    @Value("${target.nameCourse}")
    String getNameCourse();
}
