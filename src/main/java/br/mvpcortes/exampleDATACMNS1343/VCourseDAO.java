package br.mvpcortes.exampleDATACMNS1343;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VCourseDAO  extends CrudRepository<VCourse, Integer> {

    @Query("SELECT distinct v.idStudent as id_student, v.nameStudent as name_student, v.idCourse as id_course, v.nameCourse as name_course FROM #{#entityName} v WHERE v.idCourse = ?1")
    List<CourseView> getCourseView(Integer idCourse);
}
