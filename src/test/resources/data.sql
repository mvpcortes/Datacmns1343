insert into course VALUES (1, 'math');
insert into course VALUES (2, 'physics');

insert into student (id, name, course_id) VALUES (1, 'marcos', 1);
insert into student (id, name, course_id) VALUES (2, 'maria', 1);
insert into student (id, name, course_id) VALUES (3, 'joão', 1);

insert into student (id, name, course_id) VALUES (4, 'sintia', 2);
insert into student (id, name, course_id) VALUES (5, 'evelyn', 2);
insert into student (id, name, course_id) VALUES (6, 'horacio', 2);
