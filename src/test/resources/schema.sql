

create table student(
    id int not null,
    name varchar(255) not null,
    course_id int not null,
    primary key (id)
);

create table course(
    id int not null,
    name varchar(255) not null,
    primary key (id)
);

ALTER TABLE student
    ADD CONSTRAINT fk_student_course FOREIGN KEY(course_id)
    REFERENCES course(id);

create view v_course_student as
select s.id id_student, s.name name_student, c.id id_course, c.name name_course from
student s
left outer join course c on c.id = s.course_id;

