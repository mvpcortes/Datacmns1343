package br.mvpcortes.exampleDATACMNS1343;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExampleDatacmns1343ApplicationTests {

	@Autowired
	private VCourseDAO vCourseDAO;

	/**
	 * For debug
	 */
	JdbcTemplate jdbcTemplate;

	@Autowired
	private DataSource dataSource;

	@Before
	public void init(){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	@Test
	public void when_query_by_projection_then_its_works() {

		/**
		 * on spring-boot 1.5.10 it works. but on >= 1.5.11 it do not.
		 */
		List<CourseView> list = vCourseDAO.getCourseView(1);
	}

}
